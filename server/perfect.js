// const {stringify} = require('querystring')
const config = require('../config')
const db = require('schema-db')

module.exports = {
  create(to, amount) {
    const data = {
      to,
      amount,
      type: 'payment',
      status: 'created',
      system: 'perfect'
    }
    return db.table('transfer').insert(data, ['id', 'to', 'amount'])
      .then(([r]) => r)
  },

  merchant(params) {
    const q = db.table('transfer').where(params)
    console.log(q.toString())
    return q.then(function (r) {
      if (r) {
        r = r[0]
      }
      else {
        return
      }
      // const time = new Date(r.time).toUTCString()

      function url(page) {
        return `${config.origin}/perfect/${page}/${r.to}/${r.id}`
      }

      return {
        url: 'https://perfectmoney.is/api/step1.asp',
        body: {
          BAGGAGE_FIELDS: 'USER_ID TIME',
          NOPAYMENT_URL: url('fail'),
          PAYEE_ACCOUNT: config.perfect.wallet,
          PAYEE_NAME: config.name,
          PAYMENT_AMOUNT: r.amount,
          PAYMENT_ID: r.id,
          PAYMENT_UNITS: 'USD',
          PAYMENT_URL: url('success'),
          PAYMENT_URL_METHOD: 'GET',
          STATUS_URL: url('status'),
          TIME: new Date(r.time).getTime(),
          USER_ID: r.to,
          SUGGESTED_MEMO: `$${+r.amount} payment ${r.id}`
        }
      }
    })
  }
}
