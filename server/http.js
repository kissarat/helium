const express = require('express')
const db = require('./db')
const salo = require('salo')
const {merge} = require('lodash')
const perfect = require('./perfect')

const app = express()

app.get('/merchant/:to/:system/:id', function (req, res) {
  perfect.merchant(merge(req.params, {
    status: 'created',
    type: 'payment'
  }))
    .then(function (r) {
      if (r) {
        res.json(r)
      }
      else {
        res.status(404).end()
      }
    })
    .catch(salo.express(res))
})

app.use(function (req, res) {
  const agent = req.headers['user-agent']
  const time = new Date().toISOString()
  res
    .status(404)
    .type('text')
    .send(`Not Found: ${req.url}\n\n\n${agent}\n${time}\n${req.headers.ip}\n`)
})

db.connect()
  .then(function () {
    app.listen(8003)
  })
  .catch(function (err) {
    console.error(err)
    process.exit(1)
  })
