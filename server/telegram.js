const {stringify} = require('querystring')
const {request} = require('https')
const {pick, extend, isObject, each, defaults, last} = require('lodash')
const merge = require('deepmerge')

class Bot {
  constructor(options) {
    options = pick(options, 'token', 'timeout', 'offset')
    extend(this, merge({
        timeout: 90,
        offset: 0,
        chats: [],
        tasks: [],
        chat: {
          lang: 'russian'
        }
      },
      options))
  }

  request(method, params, data) {
    const dataIsObject = isObject(data)
    let url = {
      method: dataIsObject ? 'POST' : 'GET',
      hostname: 'api.telegram.org',
      path: `/bot${this.token}/${method}`
    }
    if (data) {
      data = Buffer.from(JSON.stringify(data), 'utf8')
      url.headers = {
        'content-type': 'application/json',
        'content-length': data.byteLength
      }
    }
    if (isObject(params)) {
      url.path += '?' + stringify(params)
    }
    return new Promise(function (resolve, reject) {
      const req = request(url, function (res) {
        const chunks = []
        res.on('data', function (chunk) {
          chunks.push(chunk)
        })
        res.on('end', function () {
          const data = JSON.parse((1 === chunks.length ? chunks[0] : Buffer.concat(chunks)).toString())
          if (data.ok) {
            resolve(data.result)
          }
          else {
            reject(data)
          }
        })
      })
      req.on('error', reject)
      if (dataIsObject) {
        req.write(JSON.stringify(data))
      }
      req.end()
    })
  }

  getUpdates(options = {}) {
    if (this.offset) {
      options.offset = this.offset
    }
    options.timeout = this.timeout
    return this.request('getUpdates', options)
  }

  getChatMember(chat_id, user_id) {
    return this.request('getChatMember', {chat_id, user_id})
  }

  infiniteUpdate(cb) {
    const f = () => process.nextTick(() => this.infiniteUpdate(cb))
    this.getUpdates()
      .then((r) => {
        if (r.length > 0) {
          this.offset = last(r).update_id + 1
          r.forEach(u => cb(null, u.message))
        }
        f()
      })
      .catch(function (err) {
        cb(err)
        f()
      })
  }

  sendMessage(options) {
    return this.request('sendMessage', options)
  }
}

module.exports = Bot
