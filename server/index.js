const db = require('./db')
const config = require('../config')
const Bot = require('./telegram')
const {pick} = require('lodash')
const perfect = require('./perfect')

const chats = {}

function createChat(user) {
  if (!user.username && (user.first_name || user.last_name)) {
    user.name = [user.first_name || '', user.last_name || ''].join(' ')
  }
  else {
    user.name = user.username || user.id
  }
  return chats[user.id] = user
}

function getChat(user) {
  return chats[user.id] || createChat(user)
}

function loop(bot, m) {
  const text = m.text.trim().toLocaleLowerCase()

  function send(text) {
    bot.sendMessage({chat_id: m.from.id, text})
  }

  function error(err) {
    console.error(err)
  }

  const user = getChat(m.from)
  const wallet = /(bitcoin|payeer|perfect money)/.exec(text)
  const amount = /^\$(\d+)$/.exec(text)
  console.log(user.name, text)
  if ('/start' === text) {
    const user = pick(m.from, 'id', 'first_name', 'last_name', 'username')
    user.created = new Date(m.date * 1000)
    db.table('user').insert(user).then(function () {
      send(`Здравствуй заключенный №${user.id}!`)
    })
      .catch(function (err) {
        if (db.UNIQUE_VIOLATION !== err.code) {
          console.error('User creation error', err)
        }
      })
  }
  else if (wallet) {
    user.walletName = wallet[1]
    if ('perfect money' === user.walletName) {
      user.walletName = 'perfect'
    }
    if (user.amount) {
      if ('perfect' === user.walletName) {
        perfect.create(user.id, user.amount)
          .then(function ({id, to}) {
            send(config.origin + `/pay/${to}/perfect/${id}`)
          })
          .catch(error)
      }
      else {
        send('Возможен только Perfect Money')
      }
    }
    else {
      user.handle = function (text) {
        if ('payeer' === this.walletName || 'perfect' === this.walletName) {
          text = text.toUpperCase()
        }
        db.table('user')
          .where('id', user.id)
          .update(this.walletName, text)
          .then(function () {
            send(`Кошелек ${text} сохранен`)
          })
      }
      send('Введите номер кошелька')
    }
  }
  else if (amount) {
    user.amount = +amount[1]
    send('С помощью какой платежной системы?')
  }
  else if (user.handle instanceof Function) {
    user.handle(text)
    createChat(m.from)
  }
  else {
    console.log(m)
  }
}

function main() {
  const bot = new Bot(config)
  bot.infiniteUpdate(function (err, m) {
    if (err) {
      console.error(err)
    }
    else {
      try {
        loop(bot, m)
      }
      catch (ex) {
        console.error(ex)
      }
    }
  })
}

db.connect()
  .then(main)
  .catch(function (err) {
    console.error('Database connection error', err)
    process.exit(1)
  })
