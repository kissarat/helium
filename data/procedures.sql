CREATE OR REPLACE FUNCTION cron() RETURNS VOID AS $$
DECLARE
  now TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
BEGIN
  INSERT INTO transfer ("to", "amount", "type", "status", node, "time")
    SELECT
      "user",
      amount,
      'accrue',
      'success',
      id,
      now
    FROM accrue
    WHERE amount > 0;
END;
$$ LANGUAGE plpgsql;
