CREATE DOMAIN user_id INT;

CREATE TABLE "user" (
  id         user_id PRIMARY KEY,
  username   VARCHAR(48),
  first_name VARCHAR(48),
  last_name  VARCHAR(48),
  created    TIMESTAMP,
  bitcoin    VARCHAR(48),
  payeer     VARCHAR(48),
  perfect    VARCHAR(48)
);

CREATE TABLE node (
  id      SERIAL PRIMARY KEY,
  "user"  user_id       NOT NULL REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  price   NUMERIC(7, 2) NOT NULL CHECK (price > 0),
  created TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE transfer_type AS ENUM ('payment', 'withdraw', 'accrue');
CREATE TYPE transfer_status AS ENUM ('created', 'success', 'fail');
CREATE TYPE transfer_system AS ENUM ('internal', 'bitcoin', 'payeer', 'perfect');

CREATE TABLE transfer (
  id       SERIAL PRIMARY KEY,
  "from"   user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"     user_id REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount   NUMERIC(8, 2)   NOT NULL,
  "type"   transfer_type,
  "status" transfer_status NOT NULL DEFAULT 'created',
  "system" transfer_system NOT NULL DEFAULT 'internal',
  wallet   VARCHAR(48),
  node     INT REFERENCES node (id),
  time     TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP
);
