CREATE VIEW accrue AS
  SELECT
    id,
    "user",
    (EXTRACT('days' FROM (CURRENT_TIMESTAMP - created)) * 0.03 * price) :: DECIMAL(7, 2)
    - coalesce((SELECT sum(amount)
       FROM transfer t
       WHERE t.node = n.id), 0) AS amount
  FROM node n;

CREATE OR REPLACE VIEW balance AS
  WITH t AS (
    SELECT
      "to" AS id,
      amount
    FROM transfer it
    WHERE "to" IS NOT NULL AND it.status = 'success'
    UNION ALL
    SELECT
      "from" AS id,
      -amount
    FROM transfer ot
    WHERE "from" IS NOT NULL AND ot.status = 'success'
  )
  SELECT
    id,
    sum(amount) AS balance
  FROM t
  GROUP BY id;
